package nl.practicom.c4w.txa.parser

import nl.practicom.c4w.txa.model.Application

class ApplicationParser {
    Application parse(reader) {
        return new Application()
    }
}
